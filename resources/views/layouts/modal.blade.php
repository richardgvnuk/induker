
<div class="reveal large" id="Modal_{{$post['id']}}" data-reveal>
	<h3>{{$post->title}}</h3>
	<iframe src="https://player.vimeo.com/video/{{$post['video_code']}}?autoplay=1&title=0&byline=0&portrait=0api=1&player_id=vimeo_" id="vimeo_" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
	<div class="video-bottom-decoration"></div>
  <button class="close-button" data-close aria-label="Close reveal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>


