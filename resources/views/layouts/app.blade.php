<!-- resources/views/layouts/app.blade.php -->
<?php 

if((isset($class))&& ($class=='category')){
    $size ="contain";
} else {
    $size="inherit";
}

//Definiendo el contador de acuerdo al número de posts.
if(isset($posts)) {
    $counter = count($posts);
} else {
    //$posts ="";
}

if(isset($post)){
    $post_id = $post['id'];
} else {
    $post ="";
    $post_id = "";
}


$url = url('/send');
$url_download = url('/download');

$class_total = $class.' post-sidebar';

$result = ($class=="post"?$class_total:'');

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Induker</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- CSS And JavaScript -->
        <link href='https://fonts.googleapis.com/css?family=Lato:400,700,700italic,900,900italic,300italic,300,100italic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/foundation/6.1.2/foundation.min.css">
        <link rel="stylesheet" type="text/css" href="{{ asset('stylesheets/app.css') }}">
        <script src="https://use.fontawesome.com/0dbc4fbc96.js"></script>
    </head>
    <body>
        <div class="container screen-height">
           <!-- Off Canvas Menu -->
            <div class="row with-sidebar">
                <div class="side-left large-1 medium-1 columns <?php echo $result;?>">
                    <div class="sidebar-container">
                        <div class="icon-top mobile-menu-icon"><i class="fa fa-bars" aria-hidden="true"></i></div>
                        <ul class="side-nav">
                            @if(isset($values_list))
                                @for($y=0;$y<count($posts);$y++)
                                    @if(!empty($values_list[$y]) && $values_list[$y]== $posts[$y]['id'])
                                        <li><a href='{{asset('pdf')}}/{{$posts[$y]['url']}}' target="_blank"><span class="pdf-icon active-{{$posts[$y]['id']}}"></span></a></li>
                                    @else 
                                        <li><a href='#'><span class="pdf-icon"></span></a></li>
                                    @endif
                                @endfor
                            @else
                                @if(isset($posts))
                                    @foreach($posts as $ps)
                                        <li><a href="#"><span class="pdf-icon"></span></a></li>
                                    @endforeach
                                @endif
                            @endif
                        </ul>
                        <div class="counter"><span>{{count($values_list)}}</span>/<span>{{$counter}}</span></div>
                    </div>
                    <div class="sidebar-container">
                        <div class="pdf-nivel {{(count($values_list)==4)?'complete':''}}"></div>
                        <div class="counter"><span>{{(count($values_list)==4)?1:0}}</span>/<span>1</span></div>
                    </div>
                    <?php /*--<div class="sidebar-container">
                        <ul class="navigation-menu">
                            @if(isset($values_list))
                                @for($z=0;$z<count($posts);$z++)
                                    @if(!empty($values_list[$z]) && $values_list[$z]== $posts[$z]->id)
                                        @if($post_id == $posts[$z]->id)
                                            <li><a href="#"><span class="level-icon completed active"></span></a></li>
                                        @else
                                            <li><a href="#"><span class="level-icon completed complete-{{$posts[$z]->id}}"></span></a></li>
                                        @endif
                                    @else 
                                        @if($post_id == $posts[$z]->id)
                                            <li><a href="#"><span class="level-icon completed active"></span></a></li>
                                        @else
                                            <li><a href="#"><span class="level-icon"></span></a></li>
                                        @endif
                                    @endif
                                @endfor
                            @else
                                @foreach($posts as $ps)
                                    @if($class=="welcome")
                                    
                                        <li><a href="#"><span class="level-icon"></span></a></li>
                                    
                                    @else
                                        @if($post_id == $ps->id)
                                            <li><a href="#"><span class="level-icon completed active"></span></a></li>
                                        @else
                                            <li><a href="#"><span class="level-icon"></span></a></li>
                                        @endif
                                    @endif
                                @endforeach
                            @endif
                        </ul>
                        <div class="counter"><span>0</span>/<span>{{$counter}}</span></div>
                        <div class="icon-top mobile-menu-icon" style="display:none;"><a href="{{url('/destroy')}}"><i class="fa fa-bars" aria-hidden="true"></i></a></div>
                    </div>--*/?>
                </div>
                <div class="large-11 medium-10 small-12 columns main-body">
                    <div class="container top max-width-70">
                            <div class="mobile-menu-icon">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                            </div>
                            <div class="row">
                                <div class="logo large-3 medium-3 columns">
                                    <a href="{{url('/')}}"><img src="{{asset('img/logo.png')}}" class="logo" alt=""/></a>
                                </div>
                                <nav class="navbar large-9 medium-9 columns">
                                    <ul>
                                        <li><a href="{{url('/')}}">Home</a></li>
                                        <li><a href="{{ URL::previous() }}">Volver</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    <div class="container bg-main <?php echo ($class)?$class:'';?> max-width-70 height">
                        <div class="row" >
                            @yield('content')
                        </div>
                    </div>
                </div>
                <div class="large-2 small-12 columns">
                </div>
            </div>
            <!-- /Off Canvas Menu -->
        </div>
        <!--scripts-->
        <script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
        <script src="https://cdn.jsdelivr.net/foundation/6.1.2/foundation.min.js"></script>
        <script type="text/javascript" src="{{asset('js/jszip/jszip.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/jszip/jszip-utils.min.js')}}"></script>
        <script src="{{asset('js/foundation-sites/js/foundation.reveal.js')}}"></script>
        <script src="https://f.vimeocdn.com/js/froogaloop2.min.js"></script>


        <!--/scripts-->
        <script>
            jQuery(document).ready(function($){
                var img = $('.icon-doctor > img');
                $('.icon-doctor').mouseenter(function(){
                    img.attr('src',"{{asset('img/icon_doctor_dinamic.gif')}}");
                    $('.bocadillo').fadeIn();
                }).mouseleave(function(){
                    img.attr('src',"{{asset('img/icon_doctor_static.gif')}}");
                    $('.bocadillo').fadeOut();
                })
            })
        </script>
        <script>

            jQuery(document).ready(function($){
                <?php if($class=="post"){?>
                    $(document).foundation();

                        var popup = new Foundation.Reveal($("#Modal_{{$post['id']}}"));
                        popup.open();

                    /*
                    * Vimeo Funcionality and Modal Funcionality
                    *
                    **/

                        var iframe = $('#vimeo_')[0];
                        var vimeoEl = $('#vimeo_');
                        var vimeoIndex = $('#vimeo_').attr('data-id');
                        var player = $f(iframe);
                        var status = $('.status');

                        // When the player is ready, add listeners for pause, finish, and playProgress

                        player.addEvent('ready', function() {
                            status.text('ready');
                            player.addEvent('pause');
                            player.addEvent('finish', onFinish);
                            //player.addEvent('playProgress', onPlayProgress);
                        });


                        function onFinish() {
                            popup.close();
                            $('.icon-doctor').fadeIn('slow');
                            $('.side-left').removeClass('post-sidebar');
                        }

                        $('.close-button').click(function(){
                            $('.icon-doctor').fadeIn('slow');
                            player.api('pause');
                            $('.side-left').removeClass('post-sidebar');
                        })

                        $('.reveal-overlay').click(function(){
                            $('.icon-doctor').fadeIn('slow');
                            player.api('pause');
                            $('.side-left').removeClass('post-sidebar');
                        })

                    /*
                    * Ajax Funcionality in order to set session values.
                    *
                    **/

                    $('#send_data').click(function(e){


                        var url = "<?php echo $url;?>/" + {{$post->id}};

                        //Saving the id into the session
                        $.ajax({
                          url: url
                        }).done(function(data){

                            $('#openStage').css('position','relative').animate({
                                top: '-350px',
                                opacity: 'hide', height: 'hide'},'slow',function(){
                                $('.bg-main').removeClass('post');
                            });
                            $('#completestage').animate({opacity: 'show', height: 'show'}, 'slow');
                        });



                    });


                <?php }?>

                /*
                * Sidebar Menu Funcionality
                *
                **/

                $(".mobile-menu-icon i").click(function(){

                    var navBar = $('.side-left');
                        mainBody = $('.main-body');
                        bgMain = $('.bg-main');

                    if(navBar.hasClass('mobile-menu')){

                        navBar.removeClass('mobile-menu');
                        mainBody.removeClass('mobile-stage');
                        bgMain.removeClass('mobile-stage');

                        setTimeout(function(){
                            $('html').css('overflow-x','auto');
                            $('.container.screen-height').css('overflow-x','auto');
                        },500)

                    } else {

                        $('html').css('overflow-x','hidden');
                        $('.container.screen-height').css('overflow-x','hidden');
                        navBar.addClass('mobile-menu');
                        mainBody.addClass('mobile-stage');
                        bgMain.addClass('mobile-stage');

                    }
                });

            });
        </script>

        @if($class=='post')
        <a href='#' class='back-to-top' style='display: inline;'>  
            <i class='fa fa-arrow-circle-up'></i>
        </a>
        @endif
    </body>
</html>