<!-- resources/views/tasks.blade.php -->

@extends('layouts.app')

@section('content')

    <!-- Bootstrap Boilerplate... -->

    <div class="panel-body">
        <!-- Display Validation Errors -->
        @include('errors.errors')
        <!-- New Task Form -->
        <div class="container main-content">
            <div class="row">
                    <div class="large-12 small-12 columns">
                        <div class="row">
                            <div class="large-12 small-12 columns margin-top-40 faq-icon-container">
                                <div class="row">
                                    <div class="large-2 small-12 columns"><p></p></div>
                                    <div class="large-8 small-12 text-center columns">
                                        <img src="{{asset('img/faq-icon-doctor.png')}}"/>
                                    </div>
                                    <div class="large-2 small-12 icon-doctor columns"><p></p></div>
                                </div>
                                <div class="row">
                                    <h1 class="first-header">
                                        <span class="good">"Buenas</span> personas <br>
                                        <span class="good">buenas</span> en un <span class="good">buen</span><br>
                                        lugar para trabajar"
                                    </h1>
                                    <div class="faq_underline"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-2 small-12 columns"><p></p></div>
                                <div class="large-8 small-12 columns margin-centered">
                                    <p class="faq_paragraph">Un gran lema, que define la filosofía de Grupo Indukern desde sus inicios, con una  familia fundadora que ha ido propagando sus principios año tras año.  Con el fin de conseguir un ADN propio <span class="good">(Buenas Personas)</span>, profesionales con un gran talento <span class="good">(Personas Buenas)</span> y una compañía que ofrezca una sólida estabilidad laboral <span class="good">(Un Buen lugar para trabajar)</span>.</p>
                                </div>
                            <div class="large-2 small-12 columns"><p></p></div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    <!-- TODO: Current Tasks -->
@endsection