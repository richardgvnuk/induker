<!-- resources/views/tasks.blade.php -->


@extends('layouts.app')

@section('content')

    <!-- Bootstrap Boilerplate... -->


    <div class="panel-body margin-top-40">
        <!-- Display Validation Errors -->
        @include('errors.errors')
        <!-- New Task Form -->
        <div class="container main-content margin-top-40" id="openStage" >
            <div class="row margin-top-40">
                    <div class="large-12 small-12 columns">
                        <div class="row">
                        	<div class="large-2 small-12 columns"><p></p></div>
                        	<div class="large-8 small-12 columns">
                        		<h1 class="first-header">"Buenas personas buenas</br>en un buen lugar de trabajo"<span class="header-underline"></span></h1></div>
                        	<div class="large-2 small-12 columns"><p></p></div>
                        </div>
                        <div class="row">
                            <div class="large-12 small-12 columns">
                                <div class="large-1 small-12 small-hidden columns"><p></p></div>
                                <div class="large-6 small-12 columns post-details">
                                	<h3 class="title-dec">{{$post->subtitle}}</h3>
                                	{!!$post->description!!}
                                	<div class="documento">
	                                	<div class="download-pdf">
	                        				<a href="{{asset('pdf')}}/{{$post_resources['url']}}" target="_blank" download="{{$post_resources['url']}}" id="send_data" data-id="{{$post->id}}">
                                                Descargar Adjunto
	                        				    <img src="{{asset('img/icon-download.png')}}"/>
                                            </a>
	                        			</div>
                        			</div>
                                </div>
                                <div class="large-3 small-12 columns text-center">
                                	<div class="bg-post-icon">
                                        <img src='{{asset("img/$post->avatar_name-1.jpg")}}'/>
                                    </div>
                                </div>
                                <div class="large-2 small-12 columns icon-doctor {{$class}} text-center">
                                    <img src="{{asset('img/icon-doctor.gif')}}"/>
                                </div>
                            </div>
                        </div>
                    </div>
        	</div>
        </div>
        <!-- end New Task-->
        @include('innerviews.layoutposts.completestage')

    </div>
    <!-- TODO: Current Tasks -->
    @include('layouts.modal')
@endsection