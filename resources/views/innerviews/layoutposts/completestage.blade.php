<?php

    $id = $post->id;
    if($id == 1) {
        $number ="primero";
    } elseif($id==2){
        $number = "segundo";
    } elseif($id==3){
        $number ="tercer";
    } elseif($id==4){
        $number="cuarto";
    }
?>
<div class="container main-content margin-top-40" id="completestage">
    <div class="row margin-top-40">
            <div class="large-12 small-12 columns">
                <div class="row">
                	<div class="large-2 small-12 columns"><p></p></div>
                	<div class="large-8 small-12 columns">
                		<h1 class="first-header success">¡Felicidades!<span class="header-underline"></span></h1></div>
                	<div class="large-2 small-12 columns"><p></p></div>
                </div>
                <div class="row">
                    <div class="large-12 small-12 columns">
                        <div class="large-3 small-12 small-hidden columns"><p></p></div>
                        <div class="large-6 small-12 columns post-details">
                        	<div class="success-bg">
                                <img src='{{asset("img/success-stage-$id.png")}}'/>
                            </div>
                            <h3 class="title-dec">Has completado el {{$number}} apartado con éxito.</h3>
                            	@if($id == 4)
                                    <p>No olvides descargar tu certificado por haber finalizado la experiencia</p>
                                @else
                                    <p>¡A por el siguiente capítulo!</p>
                                @endif
                        	<div class="documento">
                            	@if ($id == 4)
                                    <div class="download-pdf" style="max-width:230px;">
                                        <a href="#">Descargar Certificado</a>
                                        <img src="{{asset('img/icon-download.png')}}">
                                    </div>
                                @endif
                                <div class="download-pdf">
                                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                                    <a href="{{url('/')}}">Volver a la home</a>
                    			</div>
                			</div>
                        </div>
                        <div class="large-3 small-12 columns text-center">
                        </div>
                    </div>
                </div>
            </div>
	</div>
</div>