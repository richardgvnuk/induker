<!-- resources/views/tasks.blade.php -->

@extends('layouts.app')

@section('content')

    <!-- Bootstrap Boilerplate... -->

    <div class="panel-body">
        <!-- Display Validation Errors -->
        @include('errors.errors')
        <!-- New Task Form -->
        <div class="container main-content">
            <div class="row">
                    <div class="large-12 small-12 columns">
                        <div class="row">
                            <div class="large-2 small-12 columns"><p></p></div>
                                <div class="large-8 small-12 columns">
                                    <h1 class="first-header">{!!$page['title']!!}<span class="header-underline"></span></h1>
                                </div>
                            <div class="large-2 small-12 columns"><p></p></div>
                        </div>
                        <div class="row">
                            <div class="large-12 small-12 columns">
                                <div class="large-1 small-12 columns"><p></p></div>
                                <div class="large-8 small-12 columns">
                                @if(isset($values_list))
                                    @for($p=0;$p<count($posts);$p++)
                                        @if(!empty($values_list[$p]) && $values_list[$p]== $posts[$p]['id'])
                                            <div class="large-3 medium-3 small-12 columns each-post">
                                                <?php $default=$posts[$p];?>
                                                <a href='{{url("/posts/$default[id]")}}'>
                                                    <span class="block-icon open"></span>
                                                    <img class="img-post" src='{{asset("img/$default[avatar_name]")}}-1.jpg'/>
                                                    <span class="grey-color open">{{$default['title']}}</span>
                                                </a>
                                            </div>
                                        @else
                                             <div class="large-3 medium-3 small-12 columns each-post">
                                                <?php $default=$posts[$p];?>
                                                    @if(end($values_list) + 1 == $default['id'])
                                                        <a href='{{url("/posts/$default[id]")}}'>
                                                        <span class="block-icon bounce"></span>
                                                    @else
                                                        <a href='#'>
                                                        <span class="block-icon"></span>
                                                    @endif
                                                    <img class="img-post" src='{{asset("img/$default[avatar_name]")}}.jpg'/>
                                                    <span class="grey-color">{{$default['title']}}</span>
                                                </a>
                                            </div>
                                        @endif
                                    @endfor
                                @else
                                    @foreach($posts as $post)
                                        @if($post['id']==1)
                                            <div class="large-3 medium-3 small-12 columns each-post">
                                                <a href='{{url("/posts/$post[id]")}}'>
                                                    <span class="block-icon bounce"></span>
                                                    <img class="img-post" src='{{asset("img/$post[avatar_name]")}}.jpg'/>
                                                    <span class="grey-color">{{$post['title']}}</span>
                                                </a>
                                            </div>
                                        @else
                                            <div class="large-3 medium-3 small-12 columns each-post">
                                                <a href='#'>
                                                    <span class="block-icon"></span>
                                                    <img class="img-post" src='{{asset("img/$post[avatar_name]")}}.jpg'/>
                                                    <span class="grey-color">{{$post['title']}}</span>
                                                </a>
                                            </div>
                                        @endif
                                    @endforeach
                                @endif
                                </div>
                                <div class="large-3 small-12 icon-doctor columns">
                                    <div class="bocadillo">
                                        <p>Conoce nuestras 3B</p>
                                        <div class="bocadillo_link"><a href="{{url('/info')}}">Click aquí</a></div>
                                    </div>
                                    <img src="{{asset('img/icon_doctor_static.gif')}}" id="icon_static"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-10 small-12">
                                <div class="large-5 medium-5 small-12 margin-centered">
                                    <h3 class="title-dec">{{$page['subtitle']}}</h3>
                                    {!!$page['description']!!}
                                </div>
                            </div>
                        </div>
                    </div>
        	</div>
        </div>
    </div>
    <!-- TODO: Current Tasks -->
@endsection
