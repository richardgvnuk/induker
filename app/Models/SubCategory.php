<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    //use Translatable;

    /**
     * This model will work on the subcategories for Gebro_app. 
     * This categories will be displayed in the main page of the 
     * App.
     *
     */

    protected $table = 'subcategory';

    /**
    * This method will retrieve the category to which this
    * subcategory belongs
    *
    * @return array;
    */
    public function category(){

    	return $this->belongsTo('App\Models\Category');

    }

    /**
    * This method will retrieve all the post of the
    * to the current subcategory
    *
    * @return array;
    */

    public function post(){

    	return $this->hasMany('App\Models\Post');

    }

}
