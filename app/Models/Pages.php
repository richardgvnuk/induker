<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pages extends Model
{
    //use Translatable;

    /**
     * This model will work on the categories for Gebro_app. 
     * This categories will be displayed in the main page of the 
     * App.
     *
     */

    protected $table = 'pages';

}
