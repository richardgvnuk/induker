<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //use Translatable;

    /**
     * This model will work on the posts for Gebro_app. 
     * This posts will be displayed in the main page of the 
     * App.
     * 
     */

    protected $table = 'posts';


    /**
    * This method will retrieve all the resources blocks of the
    * to the current post.
    *
    * @return array;
	**/

    public function resources(){

    	return $this->hasMany('App\Models\PostResources');

    }


}
