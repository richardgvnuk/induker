<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostResources extends Model
{
    //use Translatable;

    /**
     * This model will work on the posts for Gebro_app. 
     * This posts will be displayed in the main page of the 
     * App.
     *
     */

    protected $table = 'posts_attachments';

    public function resources(){

    	return $this -> belongTo('App\Models\post');

    }

}
