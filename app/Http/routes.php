<?php

use App\Task;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/**
 * Show Task Dashboard
 */


//This is the main page of the Web Application
Route::get('/', 'PageController@index');
Route::get('/posts', 'PostController@getPosts');
Route::get('/send/{id}', 'SessionController@setStageValue')->where('id', '[0-9]+');
Route::get('/info','FaqController@getFaq');
Route::get('/destroy','SessionController@destroySession');



//Here we are going to define the categories page, according to the given ID
Route::group(['prefix' => 'posts'], function () { 
    Route::get('{id}',  'PostController@getPostByID')->where('id', '[0-9]+');
    Route::get('post/welcome/{id}',   'PostController@getWelcomePost')->where('id', '[0-9]+');
    Route::get('post/{id}',   'PostController@getPostByID')->where('id', '[0-9]+');
});
?>

