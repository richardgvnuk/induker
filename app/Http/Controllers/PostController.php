<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\SubCategory;
use App\Models\Post;
use App\Models\PostBlocks;
use App\Models\PostResources;
use App\Http\Controllers\Controller;
use Illuminate\Routing\Controller as BaseController;

class PostController extends BaseController
{
    //use Translatable;

    /**
     * This model will work on the posts for Gebro_app.
     * This posts will be displayed in the main page of the
     * App.
     *
     */



    /**
     * Return the the list of all posts
     *
     * @return array;
     */

    public function getPosts()
    {


        $posts = Post::all();
        $bodyClass="welcome";
        $bgBackground ="img/bg-main.png";

     

        return view('innerviews/listallposts',[
            'posts' => $posts,
            'class' => $bodyClass,
            'bgBackground' => $bgBackground
        ]);

    }


    /**
     * Return an specific post according to a given ID.
     *
     * @return array;
     */

    public function getPostByID(Request $request,$Id)
    {

        $values = $request->session()->get('values_list');
        $posts = Post::all();
        $post = Post::find($Id);
        $resources = $post->resources->all();


        $ind = $Id - 1;

        if($values){

            if($ind==0){

            } elseif(!in_array($ind, $values)){
                return view('errors/503');
            }

        } else {

            if($Id != 1){
                return view('errors/503');
            }

        }



        if($resources){
            $resources = $resources[0]['attributes'];
        } else {
            $resources['url'] = "";
        }


        $bodyClass = 'post';

        return view('innerviews/post',[
            'post' => $post,
            'class' => $bodyClass,
            'post_resources' => $resources,
            'posts'=>$posts,
            'values_list'=>$values
        ]);


    }

    /**
     * Update the data of a post according to given Id. 
     *
     * @return array;
     */

    public function updatePost($Id,$data)
    {

    }


    /**
     * Delete an specific post according to a given ID.
     *
     * @return boolean;
     */

    public function deletePost($Id)
    {
        return Post::where('id',$Id)
                    ->delete();

    }

    /**
     * Get posts according to its subcategory
     *
     * @return boolean;
     */

    public static function getPostByCategory($Id)
    {
        return Post::where('sub_category_id',$Id)
                    ->get();

    }

}
