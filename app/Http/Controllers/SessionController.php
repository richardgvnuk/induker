<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Pages;
use App\Models\Post;
use App\Http\Controllers\Controller;
use Illuminate\Routing\Controller as BaseController;


class SessionController extends BaseController
{
    //use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    /*
    * This controller will work with the data provided with Ajax
	* from the client side
    *
    **/

    /**
    * Setting a value into the session.
    * Two parameters: the key and the value.
    *
    * @return boolean;
    *
    **/

    public function setStageValue(Request $request,$value){

        $array = $request->session()->get('values_list');

        if(is_null($array)){
            $request->session()->push('values_list',$value);
        } else {
            if(!in_array($value, $array)){
                $request->session()->push('values_list',$value);
            } else {

            }
        }

        echo json_encode($request->session()->get('values_list'));
    }


    public function destroySession(Request $request){

        $request->session()->flush();

        return redirect()->action('PageController@index');
    }

}
