<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Pages;
use App\Models\Post;
use App\Http\Controllers\Controller;
use Illuminate\Routing\Controller as BaseController;

class FaqController extends BaseController
{
    //use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    public function getFaq(Request $request){

    	$request->session()->put('user_ip', getRealIP());

        //Getting the values_list
        $values = $request->session()->get('values_list');

        //Data that will be showing on the view.

        $bodyClass="faq";
        $bgmainAddClass='height';
        $bgBackground ="img/bg-main.png";

        //Retrieving post data and page data.
        $posts = Post::all();
        $page = $this::getPageByID(1);
        $page = $page['attributes'];

        $posts_final = array();

        foreach($posts as $post){
            $post_resources = Post::find($post->id);
            $resources = $post_resources->resources->all();
            $post_resources = $post_resources['attributes'];
            $post_resources['url'] = $resources[0]['attributes']['url'];
            $posts_final[] = $post_resources;
        }

        
        return view('innerviews.faq',[
            'class' => $bodyClass,
            'bgBackground'=> $bgBackground,
            'height' => $bgmainAddClass,
            'page' => $page,
            'posts' => $posts_final,
            'values_list'=>$values
         ]);
    }

    public static function getPageByID($Id)
    {

        return Pages::find($Id);

    }
}
