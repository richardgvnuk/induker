<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Pages;
use App\Models\Post;
use App\Http\Controllers\Controller;
use Illuminate\Routing\Controller as BaseController;


class PageController extends BaseController
{
    //use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    /*
    * This controller will work with the data provided by 
    * the models category.
	*
    **/

    /**
     * Return the the list of all categories
     *
     * @return array;
     */

    public function index(Request $request){

        //Setting the IP of the visitor on session.
        $request->session()->put('user_ip', getRealIP());

        //Getting the values_list
        $values = $request->session()->get('values_list');

        //Data that will be showing on the view.

        $bodyClass="welcome";
        $bgmainAddClass='height';
        $bgBackground ="img/bg-main.png";

        //Retrieving post data and page data.
        $posts = Post::all();
        $page = $this::getPageByID(1);
        $page = $page['attributes'];

        $posts_final = array();

        foreach($posts as $post){
            $post_resources = Post::find($post->id);
            $resources = $post_resources->resources->all();
            $post_resources = $post_resources['attributes'];
            $post_resources['url'] = $resources[0]['attributes']['url'];
            $posts_final[] = $post_resources;
        }

        
        return view('welcome',[
            'class' => $bodyClass,
            'bgBackground'=> $bgBackground,
            'height' => $bgmainAddClass,
            'page' => $page,
            'posts' => $posts_final,
            'values_list'=>$values
         ]);

    }

    /**
     * Return the the list of all pages
     *
     * @return array;
     */

    public function getPages()
    {

        $bodyClass = "categories background-no-position-y";

        $categories = Pages::all();

        $bgBackground ="img/bg-categories-page.png";

        return view('innerviews/allcategories', [
	        'categories' => $categories,
            'class' => $bodyClass,
            'bgBackground' => $bgBackground,
            'height' => ''
	    ]);

    }


    /**
     * Return an specific page according to a given ID.
     *
     * @return array;
     */

    public static function getPageByID($Id)
    {

        return Pages::find($Id);

    }


    /**
     * Update the data of a page according to given Id. 
     *
     * @return array;
     */

    public function updatePage($Id)
    {

    }


    /**
     * Delete an specific page according to a given ID.
     *
     * @return boolean;
     */

    public function deletePage($Id)
    {

    }

}
